# 🎲 Qui Corrige ?!

**Qui Corrige ?!** est une petite application web qui permet de désigner de manière *ludico-aléatoire* l'élève qui sera interrogé en classe. Sur cette interface, l'enseignant peut gérer plusieurs listes d'élèves, les personnaliser et suivre les tirages effectués.

👉 **Accéder à l'application :** [Qui Corrige ?!](https://lmdbt.forge.apps.education.fr/quicorrige/)  

🔙 **Version initiale :** [Découvrir la V0](https://lmdbt.forge.apps.education.fr/quicorrige/V0.html)  

## ✨ Fonctionnalités

- **📋 Multi-liste et gestion active :**  
  Gérez jusqu'à **4 listes** d'élèves simultanément. Chaque liste peut être renommée via un champ de saisie. Définissez la liste active grâce à un bouton dédié : lorsque vous sélectionnez une liste, elle passe en mode "Liste active" et les autres deviennent "Liste inactive".

- **🎰 Tirage au sort avec animation :**  
  Lancez le tirage au sort grâce à une animation dynamique qui défile rapidement les noms avant de s'arrêter sur l'élève sélectionné.

- **🔒 Tirage unique activé par défaut :**  
  Une fois tiré, l'élève est marqué avec un "#" pour indiquer qu'il a déjà été sélectionné et ne sera plus choisi durant la session. Cette option est désactivable à tout moment.

- **🧹 Réinitialisation des marques :**  
  Réinitialisez facilement les marques (le "#") en cliquant sur l'icône 🧹. Un pop-up de confirmation vous demandera d'approuver l'action avant que celle-ci ne soit effectuée.

- **📂 Chargement et enregistrement des listes :**  
  Vous pouvez charger une liste d'élèves depuis un fichier `.txt` (un nom par ligne). Les modifications sont automatiquement sauvegardées dans le **local storage** du navigateur, et vous pouvez télécharger la liste active au format `.txt`.

- **🔐 Conformité RGPD :**  
  L'application n'enregistre aucune donnée sur un serveur externe. Toutes les données restent **localement** sur votre navigateur grâce au local storage, garantissant ainsi une totale confidentialité.

- **💡 Astuce :**  
  Pour gérer plus de 4 classes/liste, utilisez **Qui Corrige ?!** dans un autre navigateur ou une fenêtre de navigation privée.

- **👀 Interface épurée et réactive :**  
  Les zones de texte contenant les listes sont repliées par défaut pour un affichage plus net. Cliquez sur l'icône (angle droit pour déployer, angle bas pour replier) pour afficher ou masquer le contenu de chaque liste.

## 🚀 Comment utiliser

1. **Saisie des noms :**  
   - Tapez directement les noms des élèves dans la zone de texte de la liste souhaitée (un nom par ligne) ou chargez une liste depuis un fichier texte.
   - Vous pouvez renommer chaque liste en modifiant le titre situé en haut de la carte.

2. **Gestion des listes :**  
   - **Déploiement/Repliement :** Cliquez sur l'icône ➡️ pour déployer ou ⬇️ pour replier la zone de texte.
   - **Sélection de la liste active :** Cliquez sur le bouton "Liste active" pour définir la liste utilisée pour le tirage. Les autres listes passent automatiquement en mode "Liste inactive".

3. **Tirage unique activé par défaut :**  
   En mode "tirage unique", chaque élève n'est tiré qu'une seule fois. Si besoin, désactivez cette option en cliquant sur le bouton dédié.

4. **Lancer le tirage :**  
   Cliquez sur le bouton 🎲 "Tirer au sort" pour démarrer l'animation. Le nom de l'élève sélectionné s'affichera à la fin de l'animation.

5. **Réinitialiser les marques :**  
   Pour effacer les marques indiquant les élèves déjà tirés, cliquez sur l'icône 🧹. Une confirmation s'affichera pour valider l'action.

6. **Télécharger la liste active :**  
   Entrez le nom de fichier souhaité et cliquez sur le bouton de téléchargement 📥 pour enregistrer la liste active en format `.txt`.

## 🛠️ Technologies utilisées

- HTML
- CSS
- JavaScript
- LocalStorage (pour la persistance des données sur le navigateur)

## 📜 Licence MIT

Ce projet est distribué sous la licence [MIT](https://fr.wikipedia.org/wiki/Massachusetts_Institute_of_Technology).  
Celle-ci permet :  
- L'utilisation libre du code source, même à des fins commerciales.  
- La modification, la redistribution et l'intégration dans d'autres projets, à condition de conserver la même licence.  
- Aucune garantie n'est fournie. L'utilisation du projet se fait sous la responsabilité de l'utilisateur.

## 👨‍💻 Auteur 

Créé par Cyril Iaconelli.  
👉 **En savoir plus :** [www.lmdbt.fr](https://www.lmdbt.fr)  

---

Merci d'utiliser **Qui Corrige ?!** et bonne chance pour vos tirages au sort en classe ! 🎯📚
